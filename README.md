# Alex Chrysovergis's Gitlab Pages

Welcome to my personal website hosted on Gitlab Pages. Here you'll find information about me, my interests as well as other cool stuff.

## Features

- **Responsive Design**: The site is optimized for both desktop and mobile devices.
- **Interactive Elements**: Includes interactive elements like games and animations.

## Techstack

- **HTML/CSS/JavaScript**
- **SCSS**
- **TypeScript**
- **Gitlab Pages**

## Installation

### Clone the repository:

`git clone https://gitlab.com/alexchrysovergis/alexchrysovergis.gitlab.io.git`

### Navigate to the src directory:

`cd src`

### Install dependencies:

`npm install`

### Build and watch for changes (automatically recompile on save):

`gulp css js compileTs watch`

### OR if you prefer to manually recompile:

`gulp css js compileTs`

---

Thank you for visiting!